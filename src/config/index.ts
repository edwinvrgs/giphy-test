const API_VERSION = 'v1';

const BASE_URL = 'https://api.giphy.com';

const ENDPOINTS = {
  trending: 'gifs/trending',
  search: 'gifs/search'
};

type Endpoints = keyof typeof ENDPOINTS;

export const API_KEY = '5Muqe6HOngq40S9xI6ZQJ7jDfvZUoS5f';

export const getEndpoint = (key: Endpoints, params: string) => {
    return `${BASE_URL}/${API_VERSION}/${ENDPOINTS[key]}?api_key=${API_KEY}&${params}`;
}

export const fetchJSONData = async (key: Endpoints, params: string = '') => {
    try {
        const response = await fetch(getEndpoint(key, params));
        return await response.json();
    } catch (e) {
        console.error(e);
    }
}
