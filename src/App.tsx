import React, {useEffect, useState} from 'react';
import './App.css';
import {fetchJSONData} from "./config";
import useFormInput from "./hooks/useFormInput";

function App() {
  const search = useFormInput('');
  const [gifs, setGifs] = useState<Array<any> | null>(null);
  const [selectedGif, setSelectedGif] = useState<any>(null);

  const onSearch = async () => {
    const {data} = await fetchJSONData('search', `q=${search.value}`);
    setGifs(data);
  }

  const fetchTrending = async () => {
    const {data} = await fetchJSONData('trending');
    setGifs(data);
  }

  useEffect(() => {
    fetchTrending();
  }, [])

  if (!gifs) {
    return <div>Loading...</div>;
  }

  return (
    <div className="App">
      <header className="App-header">
        <input {...search} type="search"/>
        <button onClick={onSearch}>Search</button>
      </header>

      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
        {
          gifs.map((gif) => {
            const { title, id, images: { fixed_height_small_still: smallImage }} = gif;

            return (
              <div key={id} style={{ cursor: 'pointer' }} onClick={() => setSelectedGif(gif.images.original)}>
                <img height={smallImage.height} width={smallImage.width} src={smallImage.url} alt={`Gif: ${title}`} />
              </div>
            )
          })
        }
      </div>

      {
        selectedGif && (
          <img style={{maxWidth: selectedGif.width, height: "auto" }} src={selectedGif.url} alt="Selected gif"/>
        )
      }
    </div>
  );
}

export default App;
